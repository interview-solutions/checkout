## Ruby - PORO Checkout

This is a PORO ([Plain old Ruby object](https://www.rubydoc.info/gems/poro-rails/0.2.0#plain-old-ruby-object)) example of a supermarket checkout, intended to display current best practices in Ruby and OOP.

### Overview
The checkout utilises the following classes:- Checkout, Item, Discount, Pence.

Most of the above is self explanatory.  The Discount class is inherited by the PercentageDiscount and BulkDiscount classes, which provide specific functionality for these discount types.

The Pence class is used to convert the operating Integer values into pounds for presentation.  All currency values are otherwise in whole pence, i.e. £1 = 100.

A Checkout is initialized in the example below, with an array of discounts, as an example.
### Usage

```ruby
item = Item.new('Apple', 5000, 5000)
discounts = [
  Discount.new(1000),
  PercentageDiscount.new(10, 9000),
  BulkDiscount.new(2500, 1, 'Apple')
]
checkout = Checkout.new(discounts)
checkout.scan(item)
price = checkout.total
```
### Documentation
Code is documented with [Yard](https://yardoc.org/).  Syntax is as follows:-
```
# @param pence [Integer] a price in pence
# @return [Integer] a price in pounds

def self.to_pounds(pence)
 pence / 100
end
```
### Running Tests

Tests are written with [RSpec](https://rspec.info/) and can be executed from the project root with `rspec spec/`.
