# frozen_string_literal: true

require_relative '../models/discount.rb'
require_relative '../models/item.rb'


RSpec.describe Discount, type: :model do

  let(:flat_discount_one) { Discount.new(1000) }
  let(:flat_discount_two) { Discount.new(7500) }
  let(:item_a_1) { Item.new('A', 5000, 5000) }
  let(:item_b_1) { Item.new('B', 3000, 3000) }
  let(:item_c_1) { Item.new('C', 2000, 2000) }
  let(:items) { [item_a_1, item_b_1, item_c_1] }

  describe '#initialize' do
    it 'initializes a Discount with total price of zero' do
      expect(flat_discount_one.value).to eq(1000)
    end
  end

  describe '#call' do
    it 'gives the discounted total' do
      expect(flat_discount_one.call(10_000, items)).to eq(9000)
    end
  end
end

RSpec.describe PercentageDiscount, type: :model do

  let(:pct_discount_one) { PercentageDiscount.new(10, 9000) }
  let(:pct_discount_two) { PercentageDiscount.new(20, 2000) }
  let(:item_a_1) { Item.new('A', 5000, 5000) }
  let(:item_b_1) { Item.new('B', 3000, 3000) }
  let(:item_c_1) { Item.new('C', 2000, 2000) }
  let(:items) { [item_a_1, item_b_1, item_c_1] }

  describe '#initialize' do
    it 'initializes a PercentageDiscount with total price of zero' do
      expect(pct_discount_one.value).to eq(10)
    end
    it 'initializes a PercentageDiscount with qualifying amount' do
      expect(pct_discount_one.qualifying_value).to eq(9000)
    end
  end

  describe '#call' do
    it 'gives the discounted total' do
      expect(pct_discount_one.call(10_000, items)).to equal(9000)
    end
  end
end

RSpec.describe BulkDiscount, type: :model do

  let(:bulk_discount_one) { BulkDiscount.new(2500, 1, 'A') }
  let(:bulk_discount_two) { BulkDiscount.new(1000, 1, 'B') }
  let(:item_a_1) { Item.new('A', 5000, 5000) }
  let(:item_b_1) { Item.new('B', 3000, 3000) }
  let(:item_c_1) { Item.new('C', 2000, 2000) }
  let(:items) { [item_a_1, item_b_1, item_c_1] }

  describe '#initialize' do
    it 'initializes a Discount with total price of zero' do
      expect(bulk_discount_one.value).to eq(2500)
    end
    it 'initializes a Discount with item name' do
      expect(bulk_discount_one.item_name).to eq('A')
    end
    it 'initializes a Discount with qualifying amount' do
      expect(bulk_discount_one.qualifying_amount).to eq(1)
    end
  end

  describe '#call' do
    it 'gives the discounted total' do
      expect(bulk_discount_one.call(10_000, items)).to equal(7500)
    end
  end
end