# frozen_string_literal: true

require_relative '../models/checkout.rb'

RSpec.describe Checkout, type: :model do

  let(:discount_one) { BulkDiscount.new(9000, 2, 'A') }
  let(:discount_two) { BulkDiscount.new(7500, 3, 'B') }
  let(:discount_three) { PercentageDiscount.new( 10, 20_000) }
  let(:discounts) { [discount_one, discount_two, discount_three] }
  let(:checkout) { Checkout.new(discounts) }
  let(:item_a_1) { Item.new('A', 5000, 5000) }
  let(:item_a_2) { Item.new('A', 5000, 5000) }
  let(:item_a_3) { Item.new('A', 5000, 5000) }
  let(:item_b_1) { Item.new('B', 3000, 3000) }
  let(:item_b_2) { Item.new('B', 3000, 3000) }
  let(:item_b_3) { Item.new('B', 3000, 3000) }
  let(:item_c_1) { Item.new('C', 2000, 2000) }
  let(:item_c_2) { Item.new('C', 2000, 2000) }
  let(:item_c_3) { Item.new('C', 2000, 2000) }

  before() do
    Checkout.send(:public, :apply_discounts)
  end

  describe '#initialize' do
    it 'initializes a Checkout with total price of zero' do
      expect(checkout.total).to equal(0)
    end
  end

  describe '#scan' do
    it 'adds items to the total basket value' do
      checkout.scan(item_a_1)
      expect(checkout.net_total).to equal(5000)
    end

    it 'adds items to the checkout items' do
      checkout.scan(item_a_1)
      expect(checkout.items[0]).to equal(item_a_1)
    end
  end

  describe '#total' do
    it 'gives the correct total for one item' do
      checkout.scan(item_a_1)
      expect(checkout.total).to equal(5000)
    end
  end

  describe '#total_pounds' do
    it 'gives the total checkout value in pounds' do
      checkout.scan(item_a_1)
      expect(checkout.total_pounds).to equal(50)
    end
  end

  describe '#apply_pricing_rules' do
    describe 'for discount_type multiple' do
      describe 'when there are not enough items required for discount' do
        it 'does not apply any discounts' do
          checkout.scan(item_a_1)
          checkout.apply_discounts
          expect(checkout.total).to equal(5000)
        end
      end

      describe 'when there are enough items required for discount' do
        it 'applies discounts' do
          checkout.scan(item_a_1)
          checkout.scan(item_a_2)
          expect(checkout.total).to equal(9000)
        end
      end

      describe 'when there are more items than required for discount' do
        it 'applies discounts, but not to any additional items' do
          checkout.scan(item_a_1)
          checkout.scan(item_a_2)
          checkout.scan(item_a_3)
          expect(checkout.total).to equal(14_000)
        end
      end
    end

    describe 'when there are multiple discount types' do
      it 'applies discounts correctly for items A, B, C' do
        checkout.scan(item_a_1)
        checkout.scan(item_b_1)
        checkout.scan(item_c_1)
        expect(checkout.total).to equal(10_000)
      end

      it 'applies discounts correctly for items B, A, B, B, A' do
        checkout.scan item_b_1
        checkout.scan item_a_1
        checkout.scan item_b_2
        checkout.scan item_b_3
        checkout.scan item_a_2
        expect(checkout.total).to equal(16_500)
      end

      it 'applies discounts correctly for items C, B, A, A, C, B, C' do
        checkout.scan item_c_1
        checkout.scan item_b_1
        checkout.scan item_a_1
        checkout.scan item_a_1
        checkout.scan item_c_2
        checkout.scan item_b_2
        checkout.scan item_c_3
        expect(checkout.total).to equal(18_900)
      end
    end
  end
end