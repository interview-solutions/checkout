# frozen_string_literal: true

# Discount applying to a group of Items, i.e. £10 discount
class Discount
  attr_reader :value

  # @param value [Integer] value of the discount, i.e. £10

  def initialize(value)
    @value = value
  end

  def call(total, items)
    [total - @value, 0].max
  end
end

# Percentage discount applying to a group of Items
class PercentageDiscount < Discount
  attr_reader :qualifying_value

  # @param qualifying_value [Integer] the amount to qualify for a discount, i.e. a total over £200 (for percentage)
  # @param value [Integer] value of the discount, i.e. 10%

  def initialize(value, qualifying_value)
    super(value)
    @qualifying_value = qualifying_value
  end

  def call(total, items)
    total > @qualifying_value ? total - (total / @value) : total
  end
end

# Bulk discount applying to a group of Items
class BulkDiscount < Discount
  attr_reader :qualifying_amount, :item_name

  # @param item_name [String] the name of the discounted Item
  # @param qualifying_amount [Integer] the amount to qualify for a discount, i.e. 3 items minimum
  # @param value [Integer] value of the discount, i.e. 3 items for £20

  def initialize(value, qualifying_amount, item_name)
    super(value)
    @item_name = item_name
    @qualifying_amount = qualifying_amount
  end

  def call(total, items)
    applicable_items = items.select { |item| item.name == @item_name }
    update_item_prices(applicable_items) if enough_items?(applicable_items)
    items.sum(&:price)
  end

  private
  # Checks if there are enough Items for a discount
  # @param items [Array<Item>] Items filtered for a discount
  def enough_items?(items)
    items&.any? && (items.count >= @qualifying_amount)
  end

  # Update Item prices with new discounted values
  # @param items [Array<Item>] Items filtered for a discount
  def update_item_prices(items)
    items_to_ignore = items.count % @qualifying_amount
    items_to_discount = items.count - items_to_ignore
    discounted_value = @value / @qualifying_amount
    items_to_discount.times do |index|
      items[index].price = discounted_value
    end
  end
end