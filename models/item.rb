# frozen_string_literal: true

# Describes an Item to be added to a checkout
class Item
  attr_reader :name
  attr_accessor :price, :base_price

  # @param name [String] the name of the discounted Item
  # @param price [Integer] the price of the item after any discounts are applied
  # @param base_price [Integer] the price of the item before any discounts are applied

  def initialize(name, price, base_price)
    @name = name
    @price = price
    @base_price = base_price
  end
end
