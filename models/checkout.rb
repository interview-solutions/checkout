# frozen_string_literal: true
require_relative 'item'
require_relative 'discount'
require_relative 'pence'

# Handles Items and calculates their total value with Discounts applied
class Checkout
  attr_reader :items, :net_total, :grand_total

  # @param discounts [Array<Discount>] list of active discounts to be applied
  def initialize(discounts)
    @grand_total = 0
    @net_total = 0
    @items = []
    @discounts = discounts
  end

  # Scans an item and adds it's price to the net_total
  # @param item [Item] Item being scanned
  def scan(item)
    @items << item
    @net_total += item.base_price
  end

  # Triggers application of all discounts and calculates the grand_total
  # @return [Integer] the resulting grand_total
  def total
    apply_discounts
    @grand_total
  end

  def total_pounds
    Pence.to_pounds(total)
  end

  private
  # Applies any active Discounts in @pricing_rules
  def apply_discounts
    if @items.any?
      @grand_total = @discounts.reduce(@net_total) { |total, discount| discount.call(total, @items) }
    end
  end
end