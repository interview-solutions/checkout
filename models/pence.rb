# frozen_string_literal: true

# Describes currency value in pence
class Pence

  # @param pence [Integer] a price in pence
  # @return [Integer] a price in pounds

  def self.to_pounds(pence)
    pence / 100
  end
end
